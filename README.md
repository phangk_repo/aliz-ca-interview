** Kevin Phang's submission**

Contact kevin.phang@outlook.sg

Features
- Initialization scripts to bootstrap environment with all dependencies
- Build custom Docker image and push to Docker registry
- Kubernetes deployment with NodePort service. Secrets are mounted as a volume to pods
- Horizontal pod autoscaler applied to Nexus deployment
- Deployment on k8s Minikube

---

## Setup GCLOUD
1. Create a project and service account. Then generate a service account key
```
https://cloud.google.com/iam/docs/creating-managing-service-accounts#creating_a_service_account

```

2. Activate service account API key
```
https://cloud.google.com/storage/docs/authentication?hl=en#service_accounts

```

3. Save the JSON at project root folder
```
gcloud-api-key.json

```

4. Create a VM instance with nested virtualization (to run Kubernetes Minikube) 
```
https://cloud.google.com/compute/docs/instances/enable-nested-virtualization-vm-instances

Debian GNU/Linux 9 (stretch)

Machine type: n1-standard-1 (1 vCPU, 3.75 GB memory)

Region: us-central1-c

Add network inbound rule for specific CIDRs to port 8081 for Nexus access. Use 0.0.0.0/0 for public access

Configure iptables on OS if necessary

```

---

## Setup on the VM instance
1. At the project root folder, run these commands to bootstrap and initialize build environment
```
sudo chmod u+x ./bootstrap.sh
sudo ./bootstrap.sh

```

2. Login to Docker
```
sudo docker login

```

3. Docker build and push. Make sure "gcloud-api-key.json" is added to project folder root
```
sudo chmod u+x ./build.sh
sudo ./build.sh

```

4. Deploy to Kubernetes Minikube
```
cat gcloud-api-key.json

sudo chmod u+x ./deploy.sh
. ./deploy.sh

```

5. Login to Nexus UI via browser
```
Get the external IP of the VM instance and access it in browser 

At port 8081 if Docker container. i.e. http://35.224.234.221:8081/
At port 32000 if Kubernetes deployment. i.e. http://35.224.234.221:32000/

Username: admin
Password: admin123 

```

6. Useful Minikube guide
```
https://kubernetes.io/docs/setup/minikube/

```

---

## Troubleshooting
```
sudo docker run -d -p 8081:8081 --name nexus -e GOOGLE_APPLICATION_CREDENTIALS='/gcloud-api-key.json' aliz-ca-interview 

sudo docker exec -it XXXXX /bin/bash

curl -u admin:admin123 http://localhost:8081/service/metrics/ping

service nexus restart

```
