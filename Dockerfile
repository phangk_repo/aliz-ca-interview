FROM sonatype/nexus3
MAINTAINER Kevin Phang
USER root
COPY nexus-blobstore-google-cloud /nexus-blobstore-google-cloud
RUN (cd /nexus-blobstore-google-cloud && sh ./install-plugin.sh /opt/sonatype/nexus)