1. Configure Jenkins to automatically to trigger a build when a push to Git occurs
2. Jenkins will build the new Docker image and push to Docker registry
3. Spinnaker can be configured to watch for changes in Docker registry and deploy latest image to Kubernetes cluster
4. Deployment to k8s cluster can be configured using rolling or blue/green deployments