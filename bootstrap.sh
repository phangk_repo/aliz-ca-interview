#!/bin/bash
LOGFILE=./bootstrap.sh.logs
echo STARTED AT $(date '+%Y %b %d %H:%M'). > $LOGFILE
apt-get update -y
apt-get upgrade -y
echo "Installing JDK." >> $LOGFILE
apt-get install openjdk-8-jre-headless -y
echo "Installing Maven." >> $LOGFILE
apt-get install maven -y
echo "Installing Docker." >> $LOGFILE
apt-get remove docker docker-engine docker.io containerd runc -y
apt-get install apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
apt-get update -y
apt-get upgrade -y
apt-get install docker-ce docker-ce-cli containerd.io -y
echo "Installing Kubernetes (Minikube)." >> $LOGFILE
apt-get install qemu-kvm -y
add-apt-repository "deb https://download.virtualbox.org/virtualbox/debian stretch contrib"
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
apt-get update -y
apt-get install virtualbox-6.0 -y
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube
cp minikube /usr/local/bin && rm minikube
apt-get update -y
apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
apt-get update -y
apt-get install -y kubectl
echo "Installing Git." >> $LOGFILE
apt-get install git -y
echo "Cloning Git repository." >> $LOGFILE
git clone https://github.com/sonatype-nexus-community/nexus-blobstore-google-cloud.git
(cd ./nexus-blobstore-google-cloud && mvn clean package "-Dsurefire.useSystemClassLoader=false")
echo COMPLETED AT $(date '+%Y %b %d %H:%M'). >> $LOGFILE