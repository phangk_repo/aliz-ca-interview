#!/bin/bash
minikube start -v=10
kubectl create secret generic gcloud-api-key --from-file=./gcloud-api-key.json
kubectl create -f kubernetes_bundle.yaml
minikube service --url aliz-ca-interviews-nexus 
